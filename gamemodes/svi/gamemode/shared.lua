GM.Name = "Snake Vs. Iguana"
GM.Author = "Wazanator"
GM.Email = "wazanator01@gmail.com"
GM.Website = "N/A"
GM.TeamBased = true

include( "player_class/player_svi.lua" )
function GM:CreateTeams()
  team.SetUp(1, "Snake", Color(255, 0, 0, 255) )
  team.SetSpawnPoint(1,"info_player_rebel")

  team.SetUp(2, "Iguana", Color(0, 255, 33, 255) )
  team.SetSpawnPoint(2,"info_player_combine")

  team.SetUp(3, "Lobby", Color(255, 255, 255, 255), false )
  team.SetSpawnPoint(3,"info_player_deathmatch")
end
