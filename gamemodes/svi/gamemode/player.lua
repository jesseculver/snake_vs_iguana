--This file contains functions related to the player
DEFINE_BASECLASS( "gamemode_base" )

--Override because we want realistic fall damage at all times
function GM:GetFallDamage(ply, speed)
  return ( speed - 526.5 ) * ( 100 / 396 ) -- the Source SDK value
end
