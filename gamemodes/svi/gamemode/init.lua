AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")
include("player.lua")
DEFINE_BASECLASS( "gamemode_base" )

local warmuptime = GetConVar("svi_warmuptime")
local testmode = GetConVar("svi_testmode")

local RoundStatus = "Warmup"

function GM:Initialize()
  Warmup()
end


function Warmup()
  RoundStatus = "Warmup"
  team.
  PrintMessage( HUD_PRINTTALK, "New round begins in ".. warmuptime:GetInt())
  timer.Simple(warmuptime:GetInt(), function()
    if(testmode:GetBool()) then
      NewRoundStart()
    else
      if (player.GetCount() < 2) then
        PrintMessage(HUD_PRINTTALK, "Not enough players present in server restarting count down")
        Warmup()
      else
        NewRoundStart()
      end
    end
  end )
end

function NewRoundStart()
  RoundStatus = "NewRoundStart"
  PrintMessage( HUD_PRINTTALK, "New round has begun, autoblancing teams")
  --Rebalance Teams
  local playerTable = { }
  table.Add(playerTable, player.GetAll())
  for i=player.GetCount(), 1, -1 do
    local tempPlayer = math.random(1,#playerTable)
    playerTable[tempPlayer]:SetTeam(team.BestAutoJoinTeam())
    PrintMessage(HUD_PRINTTALK, playerTable[tempPlayer]:Name().." joined team "..playerTable[tempPlayer]:Team())
    playerTable[tempPlayer]:Spawn()
    table.remove(playerTable,tempPlayer)
  end
end

function GM:PlayerInitialSpawn( ply )
  if(RoundStatus == "Warmup") then
    ply:SetTeam(3)
    player_manager.SetPlayerClass(ply, "player_svi")
    ply:Spawn()
  else
    ply:SetTeam(team.BestAutoJoinTeam())
    player_manager.SetPlayerClass(ply, "player_svi")
    ply:Spawn()
  end
end
